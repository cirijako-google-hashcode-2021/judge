use judge::*;
use proconio::{input, source::once::OnceSource};
use std::{collections::HashMap, io::stdin};

fn main() {
    let stdin = stdin();
    let mut cin = OnceSource::new(stdin.lock());
    input! {
        from &mut cin,
        time_limit: u64,
        _intersection_num: usize,
        street_num: usize,
        car_num: usize,
        bonus: u64,
        street_def: [(usize, usize, String, u64); street_num],
    }

    let mut street_map = HashMap::with_capacity(street_num);
    let mut streets = Vec::with_capacity(street_num);
    for (i, (_start, _end, name, length)) in street_def.into_iter().enumerate() {
        street_map.insert(name, i);
        streets.push(Street::new(length));
    }

    let mut cars = Vec::with_capacity(car_num);
    for _ in 0..car_num {
        input! {
            from &mut cin,
            n: usize,
            st: [String; n],
        }
        cars.push(Car::new(
            st.iter()
                .map(|name| *street_map.get(name).unwrap())
                .collect(),
        ));
    }

    input! {
        from &mut cin,
        intersection_num: usize,
    }
    let mut intersections = vec![vec![]; intersection_num];
    for _ in 0..intersection_num {
        input! {
            from &mut cin,
            intersection_id: usize,
            n: usize,
            st: [(String, u64); n],
        }
        for (name, delta) in st {
            intersections[intersection_id].push((*street_map.get(&name).unwrap(), delta));
        }
    }

    let mut judge = Judge {
        time_limit,
        bonus,
        streets,
        intersections: intersections
            .iter()
            .map(|i| Intersection::new(i, time_limit))
            .collect(),
        cars,
    };
    println!("{}", judge.calc());
}
