use judge::*;
use proconio::{input, source::once::OnceSource};
use std::{
    collections::{HashMap, HashSet},
    env::args,
    io::stdin,
};

fn main() {
    let m = args().skip(1).next().unwrap().parse::<u32>().unwrap();
    let stdin = stdin();
    let mut cin = OnceSource::new(stdin.lock());
    input! {
        from &mut cin,
        _time_limit: u64,
        intersection_num: usize,
        street_num: usize,
        car_num: usize,
        _bonus: u64,
        street_def: [(usize, usize, String, u64); street_num],
    }

    let mut street_map = HashMap::with_capacity(street_num);
    let mut streets = Vec::with_capacity(street_num);
    for (i, (start, end, name, length)) in street_def.into_iter().enumerate() {
        street_map.insert(name.clone(), end);
        streets.push(Street::new(length));
    }

    let mut intersections_used = vec![HashSet::new(); intersection_num];
    for _ in 0..car_num {
        input! {
            from &mut cin,
            n: usize,
            st: [String; n],
        }
        for (i, name) in st.iter().enumerate() {
            if i + 1 == n {
                continue;
            }
            let street_end = *street_map.get(name).unwrap();
            intersections_used[street_end].insert(name.clone());
        }
    }

    println!("{}", intersection_num);
    for (id, names) in intersections_used.iter().enumerate() {
        println!("{}", id);
        println!("{}", names.len());
        for name in names {
            println!("{} {}", name, m);
        }
    }
}
