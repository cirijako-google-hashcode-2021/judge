use proconio::{input, source::once::OnceSource};
use std::io::stdin;

fn main() {
    let stdin = stdin();
    let mut cin = OnceSource::new(stdin.lock());
    input! {
        from &mut cin,
        _time_limit: u64,
        intersection_num: usize,
        street_num: usize,
        _car_num: usize,
        _bonus: u64,
        street_def: [(usize, usize, String, u64); street_num],
    }

    let mut intersections = vec![vec![]; intersection_num];
    for (_start, end, name, _length) in street_def.into_iter() {
        intersections[end].push(name);
    }

    println!("{}", intersection_num);
    for (id, names) in intersections.iter().enumerate() {
        println!("{}", id);
        println!("{}", names.len());
        for name in names {
            println!("{} 1", name);
        }
    }
}
