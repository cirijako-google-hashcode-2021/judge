use std::collections::VecDeque;

#[derive(Debug, Clone)]
pub struct Judge {
    pub time_limit: u64,
    pub bonus: u64,
    pub intersections: Vec<Intersection>,
    pub streets: Vec<Street>,
    pub cars: Vec<Car>,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Intersection {
    turn_on_street: Vec<Option<usize>>,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Street {
    length: u64,
    /// Cars
    queue: VecDeque<usize>,
}

#[derive(Debug, Clone)]
pub struct Car {
    schedule: Vec<usize>,
    state: CarState,
}

#[derive(Debug, Copy, Clone)]
pub enum CarState {
    Wait,
    ArriveAt { time: u64, street_id: usize },
    Goal,
}

impl Judge {
    pub fn calc(&mut self) -> u64 {
        let mut score = 0;
        for t in 0..=self.time_limit {
            // Car
            for (car_id, car) in self.cars.iter_mut().enumerate() {
                if let CarState::ArriveAt { time, street_id } = car.state {
                    // 末端に到達
                    if t == time {
                        if car.schedule.last() == Some(&street_id) {
                            score += self.bonus + (self.time_limit - t);
                            car.state = CarState::Goal;
                        } else {
                            self.streets[street_id].queue.push_back(car_id);
                            car.state = CarState::Wait;
                        }
                    }
                }
            }
            for intersection in self.intersections.iter() {
                if let Some(street_id) = intersection.turn_on_street[t as usize] {
                    if let Some(car_id) = self.streets[street_id].queue.pop_front() {
                        let next = self.cars[car_id].next_target(street_id);
                        self.cars[car_id].state = CarState::ArriveAt {
                            time: t + self.streets[next].length,
                            street_id: next,
                        };
                    }
                }
            }
        }
        score
    }
}

impl Car {
    pub fn new(schedule: Vec<usize>) -> Car {
        let state = CarState::ArriveAt {
            time: 0,
            street_id: schedule[0],
        };
        Car { schedule, state }
    }

    pub fn next_target(&self, street_id: usize) -> usize {
        let mut iter = self.schedule.iter();
        while let Some(i) = iter.next() {
            if *i == street_id {
                return *iter.next().unwrap();
            }
        }
        panic!()
    }
}

impl Intersection {
    pub fn new(schedule: &[(usize, u64)], time_limit: u64) -> Intersection {
        let mut turn_on_street = vec![None; time_limit as usize + 1];
        if schedule.is_empty() {
            return Intersection { turn_on_street };
        }
        let mut it = schedule.iter();
        let mut t = 0;
        'OUT: while t <= time_limit {
            if let Some(&(street_id, delta)) = it.next() {
                for tt in t..t + delta {
                    if tt as usize >= turn_on_street.len() {
                        break 'OUT;
                    }
                    turn_on_street[tt as usize] = Some(street_id);
                }
                t += delta;
            } else {
                it = schedule.iter();
            }
        }
        Intersection { turn_on_street }
    }
}

impl Street {
    pub fn new(length: u64) -> Street {
        Street {
            length,
            queue: VecDeque::new(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn intersection_new() {
        let i = Intersection::new(&[(3, 1)], 5);
        assert_eq!(
            i,
            Intersection {
                turn_on_street: vec![Some(3); 6]
            }
        );

        let i = Intersection::new(&[(3, 1), (2, 2)], 5);
        assert_eq!(
            i,
            Intersection {
                turn_on_street: vec![Some(3), Some(2), Some(2), Some(3), Some(2), Some(2)],
            }
        );
    }

    #[test]
    fn example() {
        let time_limit = 6;
        let st0 = Street::new(1); // 2 0 rue-de-londres 1
        let st1 = Street::new(1); // 0 1 rue-d-amsterdam 1
        let st2 = Street::new(1); // 3 1 rue-d-athenes 1
        let st3 = Street::new(2); // 2 3 rue-de-rome 2
        let st4 = Street::new(3); // 1 2 rue-de-moscou 3
        let i0 = Intersection::new(&[(0, 1)], time_limit);
        let i1 = Intersection::new(&[(2, 2), (1, 1)], time_limit);
        let i2 = Intersection::new(&[(4, 1)], time_limit);
        let i3 = Intersection::new(&[], time_limit);
        let mut judge = Judge {
            time_limit: 6,
            bonus: 1000,
            streets: vec![st0, st1, st2, st3, st4],
            intersections: vec![i0, i1, i2, i3],
            cars: vec![Car::new(vec![0, 1, 4, 3]), Car::new(vec![2, 4, 0])],
        };
        assert_eq!(judge.calc(), 1002);
    }
}
